# fibonacci-test
A script that does the following:  
Asks for an input of how many fibonacci numbers should be printed out in the sequence.  
Prints it out in reverse order.  
Asks for an input to identify whether a number is part of the fibonacci sequence or not.


## Setup

Clone the repository:
```
git clone git@bitbucket.org:dmytro_yashchenko/fibonacci-test.git
```  
or
```
git clone https://dmytro_yashchenko@bitbucket.org/dmytro_yashchenko/fibonacci-test.git
```  
---  
Build the Docker container:
```
docker build -t fibonacci .
``` 
  
## Run

To run for the first time:
```
docker run -i --name fibonacci1 fibonacci
```  
  
To run it again:
```
docker start -i fibonacci1
```  
