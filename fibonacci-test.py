import math


# You can use it to check if n is a fibonacci number (not only part 
# of the sequence generated on the 1st step).
# Returns True if n is a fibonacci number, otherwise it returns False.
def is_fibonacci(n):
    phi = 0.5 + 0.5 * math.sqrt(5.0)
    a = phi * n
    return n == 0 or abs(round(a) - a) < 1.0 / n
# You can remove all the lines above if you only need to check whether 
# a number is part of the sequence generated on the 1st step.


try:
    fibonacci_size = int(input("Please, enter how many fibonacci numbers"
    " should be printed out in the sequence: "))
except:
    print("""
    ERROR: The entered number is invalid.
    Please, make sure that you've entered a natural number (nonnegative integer),
    and it is not less than 2.
    """)
    quit()

if fibonacci_size < 2:
    print("ERROR: The entered number shouldn't be less than 2.")
    quit()

# initializing the list and assigning first and second elements
a = [0] * fibonacci_size 
a[0] = 0 
a[1] = 1 

print("The fibonacci sequence:")
for i in range(2):
    print(a[i], end=" \n") 

for i in range(2, fibonacci_size):   
    a[i] = a[i - 2] + a[i - 1]
    print(a[i], end=" \n")  
    
print("The sequence in reverse order:")
for i in range(fibonacci_size - 1, -1 , -1):   
    print(a[i], end=" \n")  

try:
    fibonacci_check = int(input("Enter a number to check if it is part of the fibonacci sequence: "))
    print("The number", fibonacci_check, "is part of the sequence:", fibonacci_check in a)
except:
    print("""
    ERROR: The entered number is invalid.
    Please, make sure that you've entered a natural number (nonnegative integer).
    """)
